package main

import "gitlab.com/stud777/stuff/person"

func main() {

	met := person.Metodist{}

	userStr, err := met.GetUserDataString()
	if err != nil {
		panic(err)
	}

	print(userStr)

}
